---
title: "GB_disease_associatedWords_FilteredByBrainFactsNeurologicalDiseases"
author: "Shradha Mukherjee"
date: "April 3, 2019 updated April 20, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. 

## Notes for this pipeline
1) Input is the txt file format results exported from pubmed https://www.ncbi.nlm.nih.gov/pubmed using search criteria (Glioblastoma[Title/Abstract]) AND (disease[Title/Abstract] OR disorder[Title/Abstract] OR illness[Title/Abstract] OR symptom[Title/Abstract] OR side effects[Title/Abstract] OR condition[Title/Abstract])
2) Input list of neurological diseases was obtained from BrainFacts https://www.brainfacts.org/diseases-and-disorders/neurological-disorders-az
This list was edited by converting terms to all lower case, replacing , with space, keeping unique terms in excel and exporting as tab-delimited txt file 'List_of_neurological_diseases_BrainFacts'
3) Acknowledgement: This code is developed and adapted for our research from the codes:
a) Tools: use open source R packages text mining package (tm) and the word cloud generator package (wordcloud) http://www.sthda.com/english/wiki/text-mining-and-word-cloud-fundamentals-in-r-5-simple-steps-you-should-know 
b) week1 Martin Haneferd 10 mars 2017:for removing very low frequency words that create null or zeros on the matrix https://rstudio-pubs-static.s3.amazonaws.com/261827_c10d0bd464ff4726b2c940f00691b1f0.html 
c) Filtering to KeepWords of interest 
https://stackoverflow.com/questions/28069312/text-mining-with-tm-remove-words-not-in-a-list
https://stackoverflow.com/questions/47709910/tm-package-error-in-usemethodtermdocumentmatrix-x


##Step1: Install and/or Load libraries and save working directory 
```{r}
#save working directory location
original<-getwd()
original
```

```{r}
#Install packages from bioconductor, has most recent versions

#For older versions of Rstudio install packages install packages by uncommenting two lines below if packages not already installed before
#source("https://bioconductor.org/biocLite.R")
#biocLite(c("tm","SnowballC", "wordcloud", "RColorBrewer", "Matrix"))

#For new versions of Rstudio install packages install packages by uncommenting two lines below if packages not already installed before https://www.bioconductor.org/install/
#BiocManager::install(c("tm","SnowballC", "wordcloud", "RColorBrewer"))

#Alternative way of installing, uncomment line below
#install.packages(c("tm","SnowballC", "wordcloud", "RColorBrewer"))
```

```{r}
# Load installed packages
library(tm) #for text mining
library(SnowballC) #for text stemming
library(wordcloud) #word-cloud generator 
library(RColorBrewer) #color palettes
library(Matrix)
library(bigmemory)
#library(qdap)
library(gtools)
library(stringr)
```

##Step2: Load our input txt file and create corpus
```{r}
#examples of readLines function https://statistical-programming.com/r-readlines-example
text <- readLines("GB_disease_pubmed_3rdMay2019.txt")
#use a smaller test file "small_test_file.txt" to quickly test the code

#Alternative, uncomment line below to select file interactively
#text <- readLines(file.choose())
```

```{r}
# Load the data as a corpus
docs <- Corpus(VectorSource(text))
```

```{r}
#Inspect the full content of the document by uncommenting line below
#inspect(docs)
#Inspect the head of the document only
inspect(head(docs))
```

##Step3: Text transformation and cleaning the text 
```{r}
#transformation
toSpace <- content_transformer(function (x , pattern ) gsub(pattern, " ", x))
docs <- tm_map(docs, toSpace, "/")
docs <- tm_map(docs, toSpace, "@")
docs <- tm_map(docs, toSpace, "\\|")
```

```{r}
#cleaning text in one step #https://rpubs.com/williamsurles/316682
clean_corpus <- function(corpus){
  
  #Eliminate extra white spaces
  corpus <- tm_map(corpus, stripWhitespace)
  
  #Remove punctuations
  corpus <- tm_map(corpus, removePunctuation)
  
  #Convert the text to lower case
  corpus <- tm_map(corpus, content_transformer(tolower))
  
  #Remove numbers
  corpus <- tm_map(corpus, removeNumbers)
  
  #Remove common english stop words 
  corpus <- tm_map(corpus, removeWords, stopwords("english"))
  
  #Remove your own stop word
  corpus <- tm_map(corpus, removeWords, c(stopwords("en"), "medline", "pmid", "pmc", "pmcid", "hospital", "doi", "indexed", "faculty", "college", "institute", "epub", "university", "department", "usa", "author", "copyright", "journal", "school"))
  
  #uncomment line below to keep only stem or basic structure of word
  #corpus <- tm_map(corpus, stemDocument)
  
  return(corpus)
}

docs <- clean_corpus(docs)
```

#Step4: filter with list of disease words obtained from BrainFacts
```{r}
#Store the words to be used as filter as a variable KeepWords

KeepWords_import1<-read.delim("List_of_neurological_diseases_BrainFacts.txt", header=F, sep='\t')
KeepWords1<-KeepWords_import1$V1
```

```{r}
KeepPattern <- paste0(KeepWords1, collapse = "|") # create a regex pattern of the keepWords
```

```{r}
docs_keep <- unlist(str_extract_all(string = docs, pattern = KeepPattern)) # extract only the KeepWords1, as a vector
```

#optional first removal by numWords1 median
```{r}
# Calculate the number of words.
numWords1 <- sapply(gregexpr("[[:alpha:]]+", docs_keep, perl=TRUE), function(x) sum(x > 0))
# Distribution of the number of words 
summary(numWords1) #we will use the median value to filter in the next code chunk
```

```{r}
#here we calculate word frequency and remove very low frequency words that would otherwise create a large matrix with zeros or null values
length(docs_keep) #before 
docs_keep1 <- docs_keep[numWords1 > 1]
length(docs_keep1) #after remove
```

#optional second removal by numWords2 median
```{r}
# Calculate the number of words.
numWords2 <- sapply(gregexpr("[[:alpha:]]+", docs_keep1, perl=TRUE), function(x) sum(x > 0))
# Distribution of the number of words 
summary(numWords2) #we will use the median value to filter in the next code chunk
```

```{r}
#here we calculate word frequency and remove very low frequency words that would otherwise create a large matrix with zeros or null values
length(docs_keep1) #before remove
docs_keep2 <- docs_keep1[numWords2 > 2]
length(docs_keep2) #after remove
```

#Step4: Build a term-document matrix, use docs_keep or docs_keep1 or docs_keep2: Here using docs_keep

```{r}
#first define the type of source you want to use and how it shall be read
x <- VectorSource(docs_keep)
#create a corpus object
x <- VCorpus(x)
```

```{r}
#Document matrix is a table containing the frequency of the words. Column names are words and row names are documents. 
dtm <- TermDocumentMatrix(x)
dim(dtm)
is(dtm)
attributes(dtm)
```

```{r}
#If matrix still too big, then remove sparse words by uncommenting line line below
#dtm <- removeSparseTerms(dtm, 0.99)
m <- as.matrix(dtm)
v <- sort(rowSums(m),decreasing=TRUE)
d <- data.frame(word = names(v),freq=v)
head(d, 10)
#since we had kept words >5, why are we still getting freq=1 words too? These really are words that were 1+5. Likewise the other words are freq+5
write.csv(d, "Step4_result_word_frequency.csv")
```

#Step5: Generate the Word cloud
```{r}
#The importance of words can be illustrated as a word cloud as follow
set.seed(1234)
pdf(file="Step5_plot_word_cloud.pdf",height=5,width=5)
wordcloud(words = d$word, freq = d$freq, min.freq = 1,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))
dev.off()
```

#Step6: plot barplot for frequency table of words
```{r}
#View the top 20 highest frequency words
head(d, 20)
```

```{r}
#View the last 20 highest frequency words
tail(d, 20)
```

```{r}
#Plot word frequencies
pdf(file="Step6_plot_barplot_word_frequency.pdf",height=5,width=5)
barplot(d[1:20,]$freq, las = 2, names.arg = d[1:20,]$word,
col ="blue", main ="Most frequent words", cex.names=0.5, cex.axis=0.5, cex.main=0.5, cex.lab=0.5,
ylab = "Word frequencies")
dev.off()
```

```{r}
#Plot word frequencies
pdf(file="Step6_plot_density_word_frequency.pdf",height=5,width=5)
plot(density(d$freq), main="Frequency distribution of words")
polygon(density(d$freq), col="red", border="blue")
dev.off()
```

#Step7: Explore most frequent words
```{r}
summary(d$freq)
```

```{r}
#find words that occur atleast 4 times, 4 is the value of median
dtm_words<-findFreqTerms(dtm, lowfreq = 4)
head(dtm_words)
```

```{r}
#Add frequency for the words in the dtm_words with frequesncy > median
dtm_words1<-as.data.frame(dtm_words)
colnames(dtm_words1)='word'
merge_dtm_words=merge(dtm_words1, d, by='word')
write.csv(merge_dtm_words, "Step7_result_word_frequency_most_frequent_words.csv")
```

#Step8: Generate word cloud for most frequent words
```{r}
#Store the words to be used as filter as a variable KeepWords
KeepWords2<-merge_dtm_words$word
```

```{r}
#This d object we had created earlier will be filtered to KeepWords
#d <- data.frame(word = names(v),freq=v)
#head(d, 10)

#Filter d to retain KeepWords
d_filt1 <-d[(d$word)%in%KeepWords2,]
#This is same as merge_dtm_words
```

```{r}
#The importance of words can be illustrated as a word cloud as follow
set.seed(1234)
pdf(file="Step8_plot_word_cloud_most_frequent_words.pdf",height=5,width=5)
wordcloud(words = d_filt1$word, freq = d_filt1$freq, min.freq = 1,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))
dev.off()
```

#Step9: Plot barplot for table of frequency of most frequent words
```{r}
#View the top 20 highest frequency words
head(d_filt1, 20)
```

```{r}
#View the last 20 highest frequency words
tail(d_filt1, 20)
```

```{r}
#Plot word frequencies
pdf(file="Step9_plot_barplot_most_frequent_words.pdf",height=5,width=5)
barplot(d_filt1[1:20,]$freq, las = 2, names.arg = d_filt1[1:20,]$word,
col ="blue", main ="Most frequent words", cex.names=0.5, cex.axis=0.5, cex.main=0.5, cex.lab=0.5,
ylab = "Word frequencies")
dev.off()
```

#Step10: cluster of words
```{r}
f <- matrix (0, ncol=nrow(dtm), nrow=nrow(dtm))  # dtm is a term doc matrix we created before in steps above
colnames (f) <- rownames(dtm)
rownames (f) <- rownames(dtm)
```

```{r}
for (i in rownames (dtm)) {

ff <- findAssocs (dtm,i,0)

for  (j in rownames (ff)) {

  f[j,i]=ff[j,]

}

}
```

```{r}
fd <- as.dist(f) # calc distance matrix
pdf(file="Step10_plot_cluster_words.pdf",height=5,width=15)
plot(hclust(fd, method="ward.D"))  # plot dendrogram
dev.off()
```

#Step11: Partial string matching of most frequent words 'd_filt1 with full disease terms 'KeepWords1'
```{r}
#convert Keepwords1 to a data frame
KeepWords3=as.data.frame(KeepWords1)
head(KeepWords3$KeepWords1)
```

```{r}
#code from https://stackoverflow.com/questions/34720461/how-to-merge-two-data-frame-based-on-partial-string-match-with-r
idx2 <- sapply(d_filt1$word, grep, KeepWords3$KeepWords1)
idx1 <- sapply(seq_along(idx2), function(i) rep(i, length(idx2[[i]])))
d_filt1_KeepWords1<-cbind(d_filt1[unlist(idx1),,drop=F], KeepWords3[unlist(idx2),,drop=F])

write.csv(d_filt1_KeepWords1, "Step11_result_word_frequency_most_frequent_word-matched-terms.csv")
```

#Step12: Generate word cloud for most frequent word-matched-terms

```{r}
#The importance of words can be illustrated as a word cloud as follow
set.seed(1234)
pdf(file="Step12_plot_word_cloud_most_frequent_word-matched-terms.pdf",height=5,width=5)
wordcloud(words = d_filt1_KeepWords1$KeepWords1, freq = d_filt1_KeepWords1$freq, min.freq = 1,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))
dev.off()
```

#Step13: Plot barplot for table of frequency of most frequent word-matched-terms
```{r}
#View the top 20 highest frequency word-matched-terms
head(d_filt1_KeepWords1, 20)
```

```{r}
#View the last 20 highest frequency word-matched-terms
tail(d_filt1_KeepWords1, 20)
```

```{r}
#Plot word frequencies
pdf(file="Step13_plot_barplot_most_frequent_terms.pdf",height=5,width=5)
barplot(d_filt1_KeepWords1[1:20,]$freq, las = 2, names.arg = d_filt1_KeepWords1[1:20,]$KeepWords1,
col ="blue", main ="Most frequent word-matched-terms", cex.names=0.5, cex.axis=0.5, cex.main=0.5, cex.lab=0.5,
ylab = "Word frequencies")
dev.off()
```





###Save image
```{r}
#save image
save.image(file="GB_disease.RData")
```

###saving session (software version) information### 
```{r}
sessionInfo()
toLatex(sessionInfo())
```

###Clear workspace
```{r}
#clear environment to free up memory
rm(list=ls())
gc()
```
