############################################################################################################################  
							#HELLO  							 
############################################################################################################################  

#GBothers_NLP_May2019  
#title: "Glioblastoma_Comorbidities_NLP_May2019"  
#author: "Shradha Mukherjee"  
#date created: "May 4, 2019"  
#date last updated: "May 10, 2019"  
#submission: Initial Submission "May 4, 2019"  
#Citation: Glioblastoma_Comorbidities_NLP_May2019 Class Project. #Warning: This project is protected by copyright. Copyright © 2019 Mohamed Mohamed (researcher/communication), Victoria Oladoye (researcher/documentation), Sierra Murray (evaluator/appraisal), Shradha Mukherjee (researcher/scheduling) and Arizona State University for question. All rights reserved.  
  
    
############################################################################################################################  
						     #SETUP  							 
############################################################################################################################  

#####Install Anaconda on your Mac or Linux Operating System#####  
https://docs.anaconda.com/anaconda/install/index.html    
    

######Create conda environment for R and IDE RStudio#####  
#here env name being created is 'rpyide8', please use any name you like  
conda create --name rpyide8 r-base  
conda activate rpyide8  
conda config --add channels R --env  
conda config --add channels bioconda --env  
conda config --set channel_priority strict --env  
conda config --get channels --env  
#now install packages or libraries by typing following on Terminal commandlin  
conda install rstudio-desktop --name rpyide8  
#open rstudio typing 'rstudio' or open R interpretor on Terminal commandline typing 'R'. Then install additional packages, here R interpreter is shown  
R  
BiocManager::install(c("R.utils", "DT", "readr", "dplyr", "tidyr", "filesstrings"))  
BiocManager::install(c("tm", "SnowballC", "wordcloud", "RColorBrewer", "Matrix", "bigmemory", "gtools", "stringr")) 
#install additional R package 'preprocessCore' if get "Error normalize.quantiles(dataset0) : ERROR; return code from pthread_create() is 22"  
#Soution is to install 'preprocessCore', so if you're getting this error install 'preprocessCore' using single line command below  
BiocManager::install("preprocessCore", configure.args="--disable-threading", force = TRUE)  
#Type q() on rstudio console or R interpretor to exit  
q()  
#Type following to exit conda env  
conda deactivate  
  

############################################################################################################################  
						      #USE  							  							
############################################################################################################################  
  
######Commandline Usage######  
#For .sh activate either rpyide8 or rpyide9, both work  
conda activate rpyide8  
cd GBothers_NLP_May2019   
  
#For .R activate rpyide8  
conda activate rpyide8  
cd GBothers_NLP_May2019  
#To run .R script  
Rscript script_filename.R   
  

######IDE Usage######  
#For .R or .Rmd activate rpyide8  
conda activate rpyide8  
cd GBothers_NLP_May2019  
#To run .Rmd or .R script in IDE start and use rstudio, use rstudio gui to navigate/make 'GBothers_NLP_May2019' the working directory  
rstudio  
#Click File > quit to exit and close rstudio  
#Type following to exit conda env  
conda deactivate  


############################################################################################################################  
								#THANK YOU  								
############################################################################################################################  
