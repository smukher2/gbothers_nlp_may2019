disease_name
abdominal obesity-metabolic syndrome
achondrogenesis
acne inversa
acrodysostosis
adams-oliver syndrome
advanced sleep phase syndrome
agammaglobulinemia
aicardi-goutieres syndrome
alagille syndrome
alopecia  isolated
alopecia-mental retardation syndrome
alport syndrome
alternating hemiplegia of childhood
amelogenesis imperfecta
amyloidosis  primary localized cutaneous
amyotrophic lateral sclerosis
anauxetic dysplasia
anemia  congenital dyserythropoietic
anemia  hypochromic microcytic  with iron overload
anemia  sideroblastic
aneurysm  intracranial berry
aniridia
anterior segment dysgenesis
aortic aneurysm  familial abdominal
aortic aneurysm  familial thoracic
aortic valve disease
arrhythmogenic right ventricular dysplasia
arthrogryposis  renal dysfunction  and cholestasis
asperger syndrome  susceptibility to
ataxia-telangiectasia-like disorder
atelosteogenesis
atrial fibrillation  familial
atrial septal defect
atrioventricular septal defect
auditory neuropathy
auriculocondylar syndrome
autism  susceptiblity to
autoimmune disease  multisystem  infantile-onset
avascular necrosis of femoral head  primary
axenfeld-rieger syndrome
baraitser-winter syndrome
bardet-biedl syndrome
bartter syndrome
basal ganglia calcification  idiopathic
bethlem myopathy
bile acid synthesis defect  congenital
biliary cirrhosis  primary
bleeding disorder  platelet-type
blepharocheilodontic syndrome
bone marrow failure syndrome
brain small vessel disease
breast-ovarian cancer  familial  susceptibility to
breasts and/or nipples  aplasia or hypoplasia of
brittle cornea syndrome
bronchiectasis
brown-vialetto-van laere syndrome
brugada syndrome
capillary malformation-arteriovenous malformation
cardiofaciocutaneous syndrome
cardiomyopathy  familial hypertrophic
carpenter syndrome
cataract
celiac disease
cerebellar ataxia  mental retardation  and dysequilibrium
cerebral arteriopathy with subcortical infarcts and leukoencephalopathy
cerebral creatine deficiency syndrome
cerebrooculofacioskeletal syndrome
cerebroretinal microangiopathy with calcfications and cysts
ceroid lipofuscinoses
charcot-marie-tooth disease
chilblain lupus
cholestasis  benign recurrent intrahepatic
cholestasis  progressive familial intrahepatic
choroidal dystrophy  central areolar
coenzyme q10 deficiency  primary
coffin-siris syndrome
cold-induced sweating syndrome
cole-carpenter syndrome
colorectal cancer  hereditary nonpolyposis
combined oxidative phosphorylation deficiency
cone-rod dystrophy and hearing loss
cone-rod dystrophy/cone dystrophy
congenital anomalies of kidney and urinary tract
congenital disorder of glycosylation with defective fucosylation
congenital disorders of glycosylation  type i
congenital disorders of glycosylation  type ii
cornea plana
corneal dystrophy  fuchs endothelial
corneal dystrophy  posterior polymorphous
cornelia de lange syndrome
cortical dysplasia  complex  with other brain malformations
cortisone reductase deficiency
cowden disease
cranioectodermal dysplasia
craniosynostosis
cutis laxa
d-2-hydroxyglutaric aciduria
deafness  autosomal dominant
deafness  autosomal recessive
deafness  x-linked
deafness  y-linked
dent disease
dermatitis  atopic
desbuquois dysplasia
developmental dysplasia of the hip
diamond-blackfan anemia
diarrhea  congenital
dilated cardiomyopathy
duane retraction syndrome
dyschromatosis universalis hereditaria
dyskeratosis congenita
dystonia
ectodermal dysplasia (select examples)
ectodermal dysplasia and immune deficiency
ectodermal dysplasia-syndactyly syndrome
ehlers-danlos syndrome
emery-dreifuss muscular dystrophy
encephalopathy due to defective mitochondrial and peroxisomal fission
encephalopathy  acute  infection-induced
encephalopathy  progressive  early-onset  with brain edema and/or leukoencephalopathy
epidermodysplasia verruciformis  susceptibility to
epilepsy  familial adult myoclonic
epilepsy  familial focal  with variable foci
epilepsy  familial temporal lobe
epilepsy  generalized  with febrile seizures plus
epilepsy  nocturnal frontal lobe
epilepsy  progressive myoclonic
epileptic encephalopathy  early infantile
epileptic encephalopathy  infantile or early childhood
epiphyseal dysplasia  multiple
episodic ataxia
episodic kinesigenic dyskinesia
erythrocytosis  familial
erythrokeratodermia variabilis et progressiva
exudative vitreoretinopathy
facial paresis  hereditary congenital
familial adenomatous polyposis
familial candidiasis
familial cold autoinflammatory syndrome
familial episodic pain syndrome
familial restrictive cardiomyopathy
fanconi anemia
fanconi renotubular syndrome
feingold syndrome
fetal akinesia deformation sequence
fibrochondrogenesis
fibrosis of extraocular muscles  congenital
focal facial dermal dysplasia
focal segmental glomerulosclerosis
foveal hypoplasia
fraser syndrome
frontometaphyseal dysplasia
frontonasal dysplasia
frontotemporal dementia and/or amyotrophic lateral sclerosis
gallbladder disease
galloway-mowat syndrome
gaze palsy  familial horizontal  with progressive scoliosis
geleophysic dysplasia
generalized arterial calcification of infancy
giant axonal neuropathy
gingival fibromatosis
glomerulopathy with fibronectin deposits
glucocorticoid deficiency
glut1 deficiency syndrome
goiter  multinodular
granulomatous disease  chronic
griscelli syndrome
hemochromatosis
hemolytic uremic syndrome
hemophagocytic lymphohistiocytosis  familial
hennekam lymphangiectasia-lymphedema syndrome
hereditary sensory and autonomic neuropathy
hermansky-pudlak syndrome
heterotaxy  visceral
hirschsprung disease
holoprosencephaly
hot water epilepsy
hydatidiform mole  recurrent
hydrocephalus  congenital
hydrolethalus syndrome
hyper-ige recurrent infection syndrome
hyperaldosteronism
hyperbilirubinemia
hypercalcemia  infantile
hyperekplexia
hyperinsulinemia hypoglycemia
hypermanganesemia with dystonia
hyperoxaluria  primary
hyperparathyroidism
hyperphosphatasia with mental retardation syndrome
hypertropic osteoarthropathy  primary
hyperuricemic nephropathy  familial juvenile
hypocalcemia
hypocalciuric hypercalcemia
hypogonadotropic hypogonadism with or without anosmia
hypomagnesemia
hypomagnesemia  seizures  and mental retardation
hypophosphatemic rickets
hypoplastic left heart syndrome
hypospadias
hypothyroidism  congenital  nongoitrous
hypotonia  infantile  with psychomotor retardation and characteristic facies
hypotrichosis
ichthyosis  congenital  autosomal recessive
iga nephropathy
immunodeficiency (select examples)
immunodeficiency with hyper-igm
immunodeficiency  common variable
immunodeficiency-centromeric instability-facial anomalies
inclusion body myopathy/paget disease/frontotemporal dementia
infantile liver failure syndrome
infantile myofibromatosis
inflammatory bowel disease
invasive pneumococcal disease  recurrent isolated
isolated growth hormone deficiency
jervell and lange-nielsen syndrome
joubert syndrome
kabuki syndrome
kala-azar  susceptibility to
kenny-caffey syndrome
keratoconus
kleefstra syndrome
klippel-feil syndrome
leber congenital amaurosis
lectin complement activation pathway defects
left ventricular noncompaction
leopard syndrome
lethal congenital contracture syndrome
leukodystrophy  hypomyelinating
leukoencephalopathy  megalencephalic
li-fraumeni syndrome
liddle syndrome
linear skin defects with multiple congenital anomalies
lipodystrophy  congenital generalized
lipodystrophy  familial partial
lissencephaly
loeys-dietz syndrome
long qt syndrome
lymphatic malformation
lymphoproliferative syndrome
macular degeneration  age-related
macular dystrophy  patterned
macular dystrophy  vitelliform
malignant hyperthermia
mandibuloacral dysplasia with lipodystrophy
maple syrup urine disease
3mc syndrome
meckel syndrome
megalencephaly-polymicrogyria-polydactyly-hydrocephalus syndrome
meier-gorlin syndrome
melanoma  cutaneous malignant
menke-hennekam syndrome
mental retardation  autosomal dominant
mental retardation  autosomal recessive
mental retardation  nonsyndromic  x-linked
mental retardation  x-linked syndromic
methylcrotonylglycinuria
3-methylglutaconic aciduria
microcephaly and chorioretinopathy
microcephaly  growth restriction and increased sister chromatid exchange
microcephaly  primary autosomal recessive
microphthalmia  isolated
microphthalmia  isolated  with coloboma
microphthalmia  syndromic
microvascular complications of diabetes
mirror movements
mitochondrial complex i deficiency  nuclear type
mitochondrial complex iii deficiency  nuclear type
mitochondrial complex v (atp synthase) deficiency  nuclear type
mitochondrial dna depletion syndrome
mitral valve prolapse  myxomatous
miyoshi muscular dystrophy
molybdenum cofactor deficiency
mosaic variegated aneuploidy syndrome
moyamoya disease
mucopolysaccharidoses
multiple congenital anomalies-hypotonia-seizures syndrome
multiple endocrine neoplasia
multiple mitochondrial dysfunctions syndrome
multiple sclerosis  susceptibility to
multiple synostoses syndrome
muscular dystrophy  limb-girdle  autosomal dominant
muscular dystrophy  limb-girdle  autosomal recessive
muscular dystrophy-dystroglycanopathy  type a
muscular dystrophy-dystroglycanopathy  type b
muscular dystrophy-dystroglycanopathy  type c
myasthenic syndrome  congenital
myasthenic syndrome  congenital  with tubular aggregates
myoclonus  familial
myopathy  centronuclear
myopathy  lactic acidosis  and siderblastic anemia
myopathy  myofibrillar
myopathy  tubular aggregate
myopia
myotonic dystrophy
nail disorder  nonsyndromic congenital
nanophthalmos
narcolepsy
nemaline myopathy
neonatal inflammatory skin and bowel disease
nephrolithiasis/osteoporosis  hypophosphatemic
nephronophthisis
nephrotic syndrome
neu-laxova syndrome
neurodegeneration with brain iron accumulation
neuropathy  congenital hypomelinating
neutropenia  severe congenital
night blindness  congenital stationary
noonan syndrome
noonan syndrome-like disorder with loose anagen hair
nystagmus  congenital
oculocutaneous albinism
oocyte maturation defect
opitz gbbb syndrome
optic atrophy
orofacial cleft
orofaciodigital syndrome
orthostatic hypotension
osteogenesis imperfecta
osteopetrosis  autosomal dominant
osteopetrosis  autosomal recessive
otofaciocervical syndrome
otosclerosis
otospondylmegaepiphyseal dysplasia
ovarian dysgenesis
pachyonychia congenita
paget disease of bone
pancreatic agenesis
paragangliomas
parietal foramina
parkinson disease
parkinsonism-dystonia  infantile
paroxysmal nocturnal hemoglobinuria
patent ductus arteriosus
peeling skin syndrome
periventricular nodular heterotopia
peroxisome biogenesis disorder
perrault syndrome
persistent hyperplastic primary vitreous
photoparoxysmal response
pigmented nodular adrenocortical disease  primary
pituitary adenoma
pituitary hormone deficiency  combined
polycystic kidney disease
polycystic lipomembranous osteodysplasia with sclerosing leukoencephaly
polycystic liver disease
polyglucosan body myopathy
pontocerebellar hypoplasia
porokeratosis
precocious puberty  central
preeclampsia/eclampsia
preimplantation embryonic lethality
premature ovarian failure
primary ciliary dyskinesia
progeria
progressive external ophthalmoplegia with mtdna deletions
progressive familial heart block
proteosome-associated autoinflammatory syndrome
protoporphyria  erythropoietic
proximal symphalangism
pseudo-torch syndrome
pseudohypoaldosteronism  type ii
psoriasis
pulmonary fibrosis and/or bone marrow failure  telomere-related
pulmonary hypertension  primary
pulmonary venoocclusive disease
pyloric stenosis  infantile hypertrophic
pyruvate dehydrogenase complex deficiency
radioulnar synostosis with amegakaryocytic thrombocytopenia
renal hypodysplasia/aplasia
renal-hepatic-pancreatic dysplasia
restless legs syndrome
reticulate pigment disorders
retinitis pigmentosa
rhabdoid tumor predisposition syndrome
rhizomelic chondrodysplasia punctata
ritscher-schinzel syndrome
robinow syndrome
rubinstein-taybi syndrome
schwachman-diamond syndrome
schwannomatosis
sclerosteosis
seckel syndrome
seizures  benign familial infantile
seizures  benign familial neonatal
seizures  familial febrile
senior-loken syndrome
short qt syndrome
short-rib thoracic dysplasia
sick sinus syndrome
singleton-merten syndrome
skin creases  congenital symmetric circumferential
skin/hair/eye pigmentation  variation in
smith-mccort dysplasia
sotos syndrome
spastic ataxia
spastic paraplegia
spastic quadriplegic cerebral palsy
specific granule deficiency
specific language impairment
spermatogenic failure
spinal muscular atrophy with congenital bone fractures
spinal muscular atrophy  lower extremity-predominant
spinocerebellar ataxia
spinocerebellar ataxia  autosomal recessive
spinocerebellar ataxia  autosomal recessive  with axonal neuropathy
split-hand/foot malformation
spondyloarthropathy  susceptibility to
spondylocostal dysostosis
spondyloepimetaphyseal dysplasia with joint laxity
stickler syndrome
striatal degeneration  autosomal dominant
striatonigral degeneration
stuttering  familial persistent
sufactant metabolism dysfunction  pulmonary
telangiectasia  hereditary hemorrhagic
thiamine-responsive dysfunction syndrome
thiopurines  poor metabolism of
three m syndrome
thrombocythemia
thrombophilia
thyroid cancer  nonmedullary
thyrotoxic periodic paralysis
tooth agenesis  selective
townes-brocks syndrome
treacher collins syndrome
tremor  hereditary essential
trichohepatoenteric syndrome
trichothiodystrophy
trigonocephaly  isolated
tuberous sclerosis
tyrosinemia
ullrich congenital muscular dystrophy
urofacial syndrome
usher syndrome
uv-sensitive syndrome
van maldergem syndrome
vas deferens  congenital bilateral aplasia of
ventricular septal defect
ventricular tachycardia  catecholaminergic polymorphic
vesicoureteral reflux
vitamin k-dependent clotting factors  combined deficiency of
waardenburg syndrome
warburg micro syndrome
weill-marchesani syndrome
white sponge nevus
wilms tumor
xanthinuria
46 xy sex reversal
zimmermann-laband syndrome
adenoma
alopecia
alzheimer disease
anemia
aneurysm
anomalies
asthma
ataxia
autism
blood
bone mineral density
breast cancer
cardiomyopathy
"cardiomyopathy, dilated"
"cardiomyopathy, hypertrophic"
cdeficiency
cholesterol level
ciliary dyskinesia
colorectal cancer
cone-rod dystrophy
convulsions
corneal dystrophy
deafness
dementia
diabetes
diabetes mellitus
"diabetes mellitus, type 1"
"diabetes mellitus, type 2"
disorder of glycosylation
ectodermal dysplasia
ehlers-danlos
encephalopathy
epidermolysis bullosa
epilepsy
fibrosis
gastric cancer
glaucoma
glycogen storage disease
hypertension
hypogonadism
hypothyroidism
ichthyosis
immunodeficiency
lateral sclerosis
leber amaurosis
leigh syndrome
leukemia
lung cancer
lymphoma
macular degeneration
malaria
melanoma
mental retardation
microcephaly
microphthalmia
migraine
muscular dystrophy
myocardial infarction
myopathy
neuropathy
obesity
osteoporosis
ovarian cancer
pancreatic cancer
prostate cancer
rheumatoid arthritis
schizophrenia
skin/hair/eye pigmentation
stature
systemic lupus erythematosus
thyroid carcinoma
zellweger syndrome
adenocarcinoma
adrenoleukodystrophy
aids
albinism
amyloidosis
arthrogryposis
asperger syndrome
atopy
atrial fibrillation
attention-deficit hyperactivity disorder
autoimmune disease
bare lymphocyte syndrome
body mass index
brachydactyly
central hypoventilation syndrome
"ceroid-lipofuscinosis, neuronal"
cholestasis
chondrodysplasia
coloboma
colon cancer
complex i
cone dystrophy
coronary artery disease
coronary heart disease
dermatitis
dyskeratosis
dyslexia
endometrial cancer
"epiphyseal dysplasia, multiple"
fetal hemoglobin quantitative trait locus
goiter
hepatocellular carcinoma
hiv
homocystinuria
hypercholesterolemia
hypoglycemia
infections
kallmann syndrome
keratosis
leukodystrophy
leukoencephalopathy
lipodystrophy
lymphedema
macular dystrophy
major affective disorder
mucopolysaccharidosis
multiple sclerosis
myasthenic syndrome
nephropathy
neutropenia
nevus
night blindness
osteoarthritis
osteopetrosis
ovarian failure
paget disease
pheochromocytoma
polydactyly
porphyria
preeclampsia
pseudohypoaldosteronism
refsum disease
renal cell carcinoma
rickets
sarcoma
seizures
spinal muscular atrophy
squamous cell carcinoma
thalassemia
thrombocytopenia
tuberculosis
vitreoretinopathy
xeroderma pigmentosum
